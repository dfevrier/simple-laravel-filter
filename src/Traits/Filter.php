<?php

namespace dfevrier\simpleLaravelFilter\Traits;

use Illuminate\Database\Eloquent\Model;

trait Filter
{

    /**
     * @var array $queries - Contains filters to append in paginate url.
     */
    private $queries = [];

    /**
     * Apply one or multiple filters to query. Return all results.
     *
     * @param Model $model
     * @param array $columns
     * @return Model
     * @throws \Exception
     */
    public function getFilter($model, array $columns = [])
    {
        $model = $this->filter($model, $columns);
        return $model->get();
    }

    /**
     * Apply one or multiple filters to query. Return paginate results.
     *
     * @param Model $model
     * @param array $columns
     * @param int $paginate
     * @return Model
     * @throws \Exception
     */
    public function paginateFilter($model, array $columns = [], int $paginate = 5)
    {
        $model = $this->filter($model, $columns);
        return $model->paginate($paginate)->appends($this->queries);
    }

    /**
     * Apply filter to query.
     *
     * @param Model $model - Entity / Query Builder.
     * @param array $columns - Filters
     * @return Model
     * @throws \Exception
     */
    private function filter($model, array $columns)
    {
        if (!$model instanceof Model) {
            throw new \Exception('$model is not instance of Illuminate\Database\Eloquent\Model');
        }
        foreach ($columns as $key => $value) {
            if (request()->has($key) && request()->get($key) !== NULL) {
                if (is_array($value)) {
                    if (!isset($value['operator'], $value['table'])) {
                        throw new \Exception("Operator or table is not defined in App\Trait\Filter.");
                    }
                    $model = $this->addFilter($model, $key, $value['operator'], $value['table']);
                } else {
                    $model = $this->addFilter($model, $key, $value);
                }
                $this->queries[$key] = request($key);
            }
        }
        return $model;
    }

    /**
     * Add one filter.
     *
     * @param Model $model
     * @param string $key
     * @param string $operator
     * @param null|string $table
     * @return Model
     */
    private function addFilter($model, string $key, string $operator, ?string $table = null)
    {
        if (!isset($table)) {
            $table = $key;
        }
        if ($operator == 'like') {
            $model = $model->where($table, $operator ,'%'. request($key). '%');
        } else {
            $model = $model->where($table, $operator ,request($key));
        }
        return $model;
    }
}
